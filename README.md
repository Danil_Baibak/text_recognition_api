# Coding challenge - build a system for the number recognition

## Web part

Build the REST api with PHP framework Symfony 3. Currently there's only one entry point `/api`. For testing api you need to do 
POST request with field type `file` and key name `file`. You should send image with extension `.png` or `.jpg`.

The project is hosted on [heroku](https://www.heroku.com/). The project's url https://text-recognition-api.herokuapp.com/api.

The response is json. In the response body there's mandatory key - `status`, that can has two possible values: `success` and `error`. In case of error, the response will be include a list of the errors. In case of success, the response will be include a key - `number`, that would be a number, that was recognise.

For testing you can try curl request like:

```sh
curl -X POST -F 'file=@some_file_name.png' https://text-recognition-api.herokuapp.com/api
```

## Machine learnign part

A model was build using [Keras](https://keras.io/). As training data I used standart dataset, that Keras produced 
`keras.datasets.mnist`. I used simple convolutional neural network. The accuracy of the model for the testing dataset is 0.9902.

There's python script, that does image preparation like reshaping and normalization. You can find script in the folder `number_recognition`. The trained model also saved in this folder (`number_recognition/mnist.h5`).

## Future improvements

* *Model*: currently model can recognize only images from the MNIST dataset. For use different images (different shape, colors, etc.) should be improved data preparetion script and the model should be trained with mode data.
* *API*: improved error messages; handle uploaded files; logged information about images, that can't be recognized.