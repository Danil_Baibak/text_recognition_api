<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\UploadType;

class DefaultController extends Controller
{
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';


    /**
     * @Route("/api", name="api", methods={"POST"})
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(UploadType::class);
        $form->submit(['file' => $request->files->get('file')]);

        if ($form->isValid()) {
            /** @var UploadedFile $img */
            $img = $form->getData()['file'];

            $fileName = md5(uniqid()).'.'.$img->guessExtension();
            $path = __DIR__.'/../../../app/tmp';

            $img->move($path, $fileName);
            chmod($path . '/' . $fileName, 0775);

            return new JsonResponse([
                'status' => self::STATUS_SUCCESS,
                'number' => $this->recognizeNumber($fileName)
            ]);
        }

        return $this->createErrorList($form->getErrors());
    }

    /**
     * @param $fileName
     * @return string
     */
    private function recognizeNumber($fileName)
    {
        $commandPath = __DIR__ . '/../../../number_recognition/number_recognition.py';
        $command = 'python ' . $commandPath . ' ../app/tmp/' . $fileName;
        $output = shell_exec($command);

        return trim($output);
    }

    /**
     * @param $formErrors
     * @return JsonResponse
     */
    private function createErrorList($formErrors)
    {
        $errors = [];
        foreach ($formErrors as $er) {
            $errors[] = $er->getMessage();
        }

        return new JsonResponse([
            'status' => self::STATUS_ERROR,
            'errors' => $errors
        ]);
    }
}
