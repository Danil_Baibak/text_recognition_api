#!/usr/bin/python

import os
import sys
import cv2
import keras
from keras.models import load_model
import numpy

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class NumberRecognition(object):
    img_rows = 28
    img_cols = 28

    def __init__(self):
        # set root path for the project
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        self.model = load_model('mnist.h5')

    def prepare_image(self, file_path):
        """
        Reshape and normalize image
        :param file_path:
        :return:
        """
        image = cv2.imread(file_path, 0)
        image = image.reshape(1, self.img_rows, self.img_cols, 1).astype('float32')
        self.image = image / 255

        return self

    def recognise(self):
        """
        Find number on the image
        :return: the integer - number, that model recognizes on the image
        """
        return self.model.predict_classes(self.image, verbose=0)[0]


if __name__ == '__main__':
    number = NumberRecognition().prepare_image(sys.argv[1]).recognise()
    print(number)
