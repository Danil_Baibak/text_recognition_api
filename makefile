init_composer: composer.phar
	php composer.phar install --prefer-dist -o --no-interaction

composer.phar:
	curl -s https://getcomposer.org/installer | php

init_permissions:
	mkdir app/cache app/logs app/spool app/tmp app/var/data/certificate_image -p
	sudo /bin/setfacl -R -m u:www-data:rwX -m u:it:rwX app/cache app/logs app/spool app/tmp
	sudo /bin/setfacl -dR -m u:www-data:rwX -m u:it:rwX app/cache app/logs app/spool app/tmp
	sudo /bin/setfacl -R -m u:www-data:rwX -m u:it:rwX app/var/data
	sudo /bin/setfacl -dR -m u:www-data:rwX -m u:it:rwX app/var/data
	sudo /bin/setfacl -dR -m u:www-data:rwX -m u:it:rwX var/cache/

clean_cache:
	rm -Rf var/cache/*
	rm -Rf app/cache/*
	rm -Rf app/logs/*
	echo "flush_all" | nc -q 2 memcached 11211

clean:
	make clean_cache
	rm -rf app/tmp/*
